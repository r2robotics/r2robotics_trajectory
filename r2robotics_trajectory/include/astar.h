// Implementation inspired by :
// https://www.redblobgames.com/pathfinding/a-star/implementation.html

#ifndef ASTAR
#define ASTAR

#include "pose.h"
#include "angles.h"

#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <math.h>
#include <stdexcept>
#include <limits>

enum EdgeType {
    STRAIGHT = 0,
    ROTATION = 1,
    CIRCLE = 2
};

template<typename T, typename priority_t>
struct PriorityQueue {
  typedef std::pair<priority_t, T> PQElement;
  std::priority_queue<PQElement, std::vector<PQElement>,
                 std::greater<PQElement>> elements;

  inline bool empty() const {
     return elements.empty();
  }

  inline void put(T item, priority_t priority) {
    elements.emplace(priority, item);
  }

  T get() {
    T best_item = elements.top().second;
    elements.pop();
    return best_item;
  }
};

struct Node
{
    Node() : id(""), x(0), y(0), yaw(0), final_yaw_needed(true) {}
    Node( std::string id_, int x_, int y_, double yaw_ ): 
        id(id_), x(x_), y(y_), yaw(yaw_), final_yaw_needed(true) {}
    Node( std::string id_, double x_, double y_, double yaw_ ): 
        id(id_), x(x_), y(y_), yaw(yaw_), final_yaw_needed(true) {}
    std::string id;
    void set_final_yaw_needed( bool val )
    {
        final_yaw_needed = val;
    }
    int x;
    int y;
    double yaw;
    bool final_yaw_needed; // if false, we don't ware about the yaw at this node
};

struct Edge
{
    Edge() {}
    Edge( std::string n1_, std::string n2_ ): 
        n1(n1_), n2(n2_), type(STRAIGHT) {} 
    Edge( std::string n1_, std::string n2_, EdgeType type_ ): 
        n1(n1_), n2(n2_), type(type_) {} 
    Edge( std::string n1_, std::string n2_, EdgeType type_,
            int dir, double _x, double _y ): 
        n1(n1_), n2(n2_), type(type_), direction(dir),
        center_x(_x), center_y(_y), final_yaw(0) {} 
    std::string n1;
    std::string n2;
    EdgeType type;
    int direction; // +1 pour aller tout droit, -1 pour aller derriere (robot directionel)
    double final_yaw; // radian
    double center_x;
    double center_y;
};

typedef std::vector< std::string > NodeTrajectory;
typedef std::vector< Edge > EdgeTrajectory;

struct Graph 
{
    Graph() {}
    Graph( std::map< std::string, Node > nodes_ ):
        nodes(nodes_)
    {}
    Graph( std::vector< Edge > edges_, std::map< std::string, Node > nodes_ ):
        edges(edges_), nodes(nodes_) 
    {
        calculateNeighbors();
    }

    std::vector< Edge > edges;
    std::map< std::string, Node > nodes;
    std::map< std::string, std::set< std::string > > neighbors;

    void calculateNeighbors()
    {
        for (auto e : edges)
        {
            // check if keys exists, if not, create an empty vector
            if ( neighbors.find(e.n1) == neighbors.end() )
            {
                neighbors[e.n1] = std::set< std::string > ();
            }
            if ( neighbors.find(e.n2) == neighbors.end() )
            {
                neighbors[e.n2] = std::set< std::string > ();
            }
            // add the goal neighbor to the first node's neighbors set
            neighbors.at(e.n1).insert(e.n2);
            // neighbors.at(e.n2).insert(e.n1);
            // if it's an arc-circle edge, update final yaw with yaw of n2
            if( e.type == CIRCLE )
            {
                e.final_yaw = node(e.n2).yaw;
                std::cout << "updating final yaw ARC CIRCLE" << e.final_yaw << "\n";
            }
        }
    }

    Node node( std::string id )
    {
        if( nodes.count(id) > 0)
        {
            return nodes.at( id );
        }
        else
        {
            return Node();
        }
    }

    std::set< std::string > getNeighbors( std::string n )
    {
        return neighbors.at( n );
    }

    double cost( std::string start, std::string end )
    {
        // TO DO
        return 1;
    }

    double distanceSquared( int x1, int y1, int x2, int y2 )
    {
        return pow(x1-x2,2) + pow(y1-y2,2);
    }

    double distanceSquared( Pose p, std::string id )
    {
        Node n = node(id);
        return distanceSquared(p.x,p.y, n.x, n.y);
    }
    double distanceAngleDegrees( Pose p, std::string id )
    {
        Node n = node(id);
        return fabs( ADD_DEG(p.yaw, -n.yaw) );
    }

};

class AStar
{
    private:
        Graph graph;
        double heuristic( std::string a, std::string b);

    public:
        AStar () {}
        AStar ( std::map< std::string, Node > nodes_ )
        {
            graph = Graph( nodes_ );
        }
        AStar ( std::vector< Edge > edges_, std::map< std::string, Node > nodes_ )
        {
            graph = Graph( edges_, nodes_ );
        }
        NodeTrajectory search( std::string start, std::string goal );
        EdgeTrajectory nodeTrajectoryToEdgeTrajectory( NodeTrajectory node_traj );
        Node node( std::string id )
        {
            return graph.node( id );
        }
        Edge edgeBetween( std::string n1, std::string n2 );
        std::string closestNode( Pose p, double &d );
        double distance( std::string n1, std::string n2 );
        void addNode( std::string n, int x, int y, int yaw, std::string neighbor );
        void deleteNode( std::string n );

};

#endif //ASTAR