#ifndef ANGLES
#define ANGLES

#include <cmath>
#include <iostream>
#include <vector>

// Fallback, in case M_PI doesn't exist.
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

// Conversion functions to make the code below more readable.
double DEG2RAD(double DEG)
{
  return DEG * M_PI / 180.0;
}

double RAD2DEG(double RAD)
{
  return RAD * 180.0 / M_PI;
}

double DEG_TO_180(double DEG)
{
  if(DEG <= -180.0)
    return DEG_TO_180(DEG+360.0);
  else if(DEG > 180.0)
    return DEG_TO_180(DEG-360.0);
  else
    return DEG;
}

double ADD_DEG(double DEG1, double DEG2)
{
  double DEG = DEG1 + DEG2;
  return DEG_TO_180(DEG);
}

double ADD_DEG_360(double DEG1, double DEG2)
{
  double DEG = DEG1 + DEG2;
  if (DEG > 360.0)
    return DEG - 360.0;
  else if(DEG <= 0.0)
    return DEG + 360.0;
  else
    return DEG;
}

bool is_angle_between(double theta, double tmin, double tmax)
{
  // angles between -180 et 180
  if ( tmin <= 0.0 && tmax >= 0.0 )
    return ( (0.0 <= theta  && theta <= tmax) || (0.0 >= theta && theta >= tmin) );
  else if ( tmin >= 0.0 && tmax >= 0.0 && tmin <= tmax)
    return ( tmin <= theta && theta <= tmax );
  else if ( tmin >= 0.0 && tmax >= 0.0 && tmin > tmax)
    return ( !(is_angle_between(theta, tmax, tmin)) );
  else if (tmin <= 0.0 && tmax <= 00 && tmin <= tmax)
    return ( theta >= tmin && theta <= tmax);
  else if (tmin <= 0.0 && tmax <= 0.0 && tmin > tmax)
    return (!(is_angle_between(theta, tmax, tmin)));
  else // tmin >= 0 && tmax <= 0
    return ( (tmin <= theta && theta <= 180.0) || (-180.0 <= theta && theta <= tmax) );
}

std::vector< int > get_angles_between(double tmin, double tmax, int step)
{
  std::vector<int > angles;
  for(int t=-180; t<180; t+= step ) {
    if(is_angle_between((double)t,tmin,tmax) ) {
      angles.push_back(t);
    }
  }
  return angles;
}

double get_angle_between(double DEG1, double DEG2)
{
  return DEG_TO_180( DEG_TO_180(DEG1) - DEG_TO_180(DEG2) );
}

bool CLOSECAP( double cap1, double cap2, double EPSILON_CAP )
{
  return fabs( ADD_DEG(cap1, -cap2) ) < EPSILON_CAP;
}

#endif
