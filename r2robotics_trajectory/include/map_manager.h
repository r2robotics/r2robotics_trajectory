#ifndef MAP_MANAGER
#define MAP_MANAGER

#include "ros/ros.h"

#include <vector>
#include <map>
#include <ros/package.h>
#include <math.h>

#include "fcl/fcl.h"
#include "fcl/math/bv/OBBRSS.h"

#include "collision_manager.h"
#include "potential_field.h"
#include "astar.h"
#include "pose.h"
#include "load_obj_file.h"
#include "trajectory_variables.h"
#include "angles.h"

#include <r2robotics_msgs/Movement.h>
#include <r2robotics_msgs/Velocity.h>

typedef std::vector< r2robotics_msgs::Movement > Trajectory;
typedef std::vector< Edge > EdgeTrajectory;
typedef std::map< std::string, Node > GraphPoints;
typedef r2robotics_msgs::Velocity Velocity;
typedef r2robotics_msgs::Movement Movement;
typedef fcl::BVHModel< fcl::OBBRSS<double> > Model;

class MapManager
{
  public:
    MapManager() {}
    MapManager( ros::NodeHandle node );

    bool initPose( Pose start );
    bool setCurrentPose( Pose newPose );
    bool setGoalPoint( std::string pointID );
    bool setGoalPoint( Pose targetPose );
    bool loadRobot();
    bool loadTable();
    bool loadGraph();
    Trajectory calculateAndGetGlobalTrajectory();
    Trajectory getGlobalTrajectory();
    Velocity calculateAndGetVelocity();
    Velocity getVelocity();
    Node node( std::string id );
    std::string printDistanceToObstacles();

  private:
    ros::NodeHandle nh;

    PotentialField potentialField;
    CollisionManagerPtr_t collisionManager;
    // TableVisualization *TableVisualization;
    AStar astar;

    double GRAPH_MIN_DISTANCE;
    bool OMNIDIRECTIONAL;

    Pose currentPose;
    Pose previousPose;
    std::string targetPointID;
    Pose targetPose;
    Trajectory currentTrajectory;
    Velocity currentVelocity;
    std::map< std::string, Node > points;
    double MIN_DISTANCE_CONTROL;

    bool calculateGlobalTrajectory();
    bool calculateVelocity();
    Velocity getOptimalVelocity( Pose current, Pose target );
    void printNodeTrajectory( NodeTrajectory nodetraj );
    void printTrajectory( Trajectory traj );
    void printMovement( r2robotics_msgs::Movement move );
    Trajectory edgeTrajectoryToMovementTrajectory( EdgeTrajectory edge_traj );
    CollisionPartsPtr_t loadCollisionParts( std::string prefix );
    CollisionPartPtr_t loadModelFile( std::string package, std::string file );
    CollisionPartPtr_t load3DModel( std::string type, std::string prefix );
    std::map< std::string, Node > loadGraphNodes();
    std::vector< Edge > loadGraphEdges();
    std::vector< Edge > loadNeighborNodesEdges( int i );

    bool testCollision();
    Pose getCurrentPose();
    std::string getCurrentPointID();

    bool setEnnemyPositions();
    bool setEnnemyRobots();
};

#endif //MAP_MANAGER