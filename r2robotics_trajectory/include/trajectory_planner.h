#ifndef TRAJECTORY_PLANNER
#define TRAJECTORY_PLANNER

#include "ros/ros.h"
#include <actionlib/server/simple_action_server.h>
#include <actionlib/client/simple_action_client.h>
#include <tf/LinearMath/Quaternion.h>
#include <tf/LinearMath/Matrix3x3.h>

#include "map_manager.h"
#include "pose.h"
#include "trajectory_variables.h"
#include "angles.h"

#include <r2robotics_msgs/Movement.h>
#include <r2robotics_msgs/Velocity.h>
#include <r2robotics_msgs/Position.h>
#include <r2robotics_msgs/RobotPosition.h>
#include <gazebo_msgs/LinkStates.h>
#include <gazebo_msgs/LinkState.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/Vector3.h>

#include <std_srvs/Trigger.h>
#include <std_srvs/SetBool.h>
#include <r2robotics_srvs/set_position.h>
#include <r2robotics_srvs/GoToPoint.h>
#include <r2robotics_srvs/stopControl.h>
#include <r2robotics_srvs/resumeControl.h>

#include <r2robotics_actions/TargetPointAction.h>
#include <r2robotics_actions/ConsigneAction.h>

typedef r2robotics_msgs::Velocity Velocity;

enum ActionType {
  Nothing,  // Do nothing
  GoTo,     // Go to a pose (x,y,cap)
  Move      // Do a relative movement
};

enum TrajectoryMode {
  LOCAL_PATH_PLANNING  = 0,    // Use the local path planning method
  GLOBAL_PATH_PLANNING = 1,   // Use the global path planning method
};

// A trajectory is a vector of movements
typedef std::vector< r2robotics_msgs::Movement > Trajectory;
typedef r2robotics_msgs::Velocity Velocity;
typedef actionlib::SimpleActionServer<r2robotics_actions::TargetPointAction> TargetPointServer;
typedef actionlib::SimpleActionServer<r2robotics_actions::ConsigneAction> ConsigneServer;
typedef actionlib::SimpleActionClient<r2robotics_actions::TargetPointAction> TargetPointClient;

class TrajectoryPlanner
{
  public:
    TrajectoryPlanner( ros::NodeHandle node );
    
    // initialization
    void initPose( int x, int y, double cap );
    bool loadRobot();
    bool loadTable();
    bool loadGraph();
    // Sends the immediate action for the control node
    // (method to be called at each loop by the Trajectory node).
    void loop();

  private:
    // ROS variables
    std::string node_name;
    ros::NodeHandle nh;
    bool GAZEBO;
    
    MapManager mapManager;
    // TableVisualization tableVisualization;
    Pose currentPose;
    Pose targetPose;
    Pose waypointPose;

    void setCurrentPose( int x, int y, double yaw );
    void simulatorSetPosition( double x, double y, double yaw );
    void simulatorForceZPose();
    void firmwareSetPosition( int x, int y, double cap );
    // Current trajectory mode (global or local)
    TrajectoryMode trajectoryMode;
    // Action currently active 
    // ie Nothing, GoTo (absolute movement) or Move (relative movement)
    ActionType currentAction;
    // Global trajectory calculated for the current action
    Trajectory currentTrajectory;
    // Local trajectory (velocity) calculated for the current action
    Velocity currentVelocity;
    // True if a trajectory has been calculated and sent for the current action 
    bool action_sent;
    bool already_reached;
    bool ERROR_STOP;

    // Topic publishers
    ros::Publisher trajectory_topic;
    ros::Publisher velocity_topic;
    ros::Publisher simulator_pose_publish;
    ros::Subscriber current_position_topic;
    ros::Subscriber simu_pose;
    // Service clients
    ros::ServiceClient set_position_firmware_client;
    ros::ServiceClient stop_control_client;
    ros::ServiceClient resume_control_client;
    ros::ServiceServer waypoint_reached;
    ros::ServiceServer set_position;
    ros::ServiceServer go_to_point;
    ros::ServiceServer as_if_done;
    ros::ServiceServer set_ERROR_STOP_srv;
    // Action servers
    TargetPointServer targetpoint_server;
    ConsigneServer consigne_server;
    // Action clients
    TargetPointClient targetpoint_client;
    // Action goal to store goal sent from strategy
    r2robotics_actions::TargetPointGoal targetpoint_goal;
    r2robotics_actions::ConsigneGoal consigne_goal;

    // Initialization methods
    void initTopics();
    void initServices();
    void initActions();
    // return if the current position is equal to the goal position
    bool targetReached();

    // Send the next movement in the trajectory to the Control system
    bool sendNextMovement();

    // Determine if two double are closer than a given distance
    bool close( double x1, double x2, double delta );
    // Determine if two double are close with a certain epsilon
    bool close( double x1, double x2 );
    // Determine if two caps (degree) are close with a certain epsilon
    bool closecap( double cap1, double cap2 );
    // Determine if two Poses are close to each other
    bool close( Pose p1, Pose p2 );
    // Determine if two points determined by (x,y) are close to each other
    bool close( double x1, double y1, double x2, double y2);
    // Determine if a movement is static
    bool isMovementStatic( r2robotics_msgs::Movement path );
    bool checkIfTheRobotIsApproximatelyWhereWeWant();

    // Callback functions for topics
    void currentPositionCallback(const r2robotics_msgs::Position pos);
    void targetPositionCallback(const r2robotics_msgs::RobotPosition pos);
    void linkStatesCallback(const gazebo_msgs::LinkStates feedback);
    // Callback functions for services
    bool triggerNextMovement(std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res);
    bool trajectory_set_position(r2robotics_srvs::set_position::Request& req, r2robotics_srvs::set_position::Response& res);
    bool trajectory_go_to_point(r2robotics_srvs::GoToPoint::Request& req, r2robotics_srvs::GoToPoint::Response& res);
    bool trajectory_as_if_done ( std_srvs::Trigger::Request& req, std_srvs::Trigger::Response& res );
    bool set_ERROR_STOP ( std_srvs::SetBool::Request& req, std_srvs::SetBool::Response& res );
    // Callback functions for actions
    void targetpoint_goal_CB();
    void targetpoint_preempt_CB();
    void consigne_goal_CB();
    void consigne_preempt_CB();

    Node node( std::string id );
};

#endif