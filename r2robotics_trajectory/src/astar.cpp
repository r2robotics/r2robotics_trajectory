#include "astar.h"

using namespace std;

double AStar::heuristic( string name_a, string name_b ) 
{
    Node a = node( name_a );
    Node b = node( name_b );
    return sqrt( pow(a.x - b.x, 2) + pow(a.y - b.y, 2) );
}

Edge AStar::edgeBetween( string n1, string n2 )
{
    Edge res;
    // Return an edge from n1 to n2 
    for (Edge e : graph.edges)
    {
        if( e.n1 == n1 && e.n2 == n2 )
        {
            return e; // return first edge to next node, to change later
        }
    }
    throw std::invalid_argument( "Nodes are not connected in the graph" );
}

string AStar::closestNode( Pose p, double &d )
{
    string id_closest = "";
    d = numeric_limits<double>::max();
    double angle_d = 180; // 180 ° degrees max diff in angle
    for(auto n : graph.nodes)
    {
        double newd = graph.distanceSquared(p, n.first);
        double newangle_d = graph.distanceAngleDegrees(p, n.first);
        if( (newd < d) | ( newd == d && newangle_d <= angle_d ) )
        {
            d = newd;
            angle_d = newangle_d;
            id_closest = n.first;
        }
    }
    return id_closest;
}

EdgeTrajectory AStar::nodeTrajectoryToEdgeTrajectory( NodeTrajectory node_traj )
{
    EdgeTrajectory edge_traj;
    for(NodeTrajectory::iterator it = node_traj.begin(); it != node_traj.end()-1; ++it) {
        auto next_it = next(it); // get next node in the trajectory
        edge_traj.push_back( edgeBetween(*it, *next_it) );        
    }
    return edge_traj;
}

NodeTrajectory AStar::search( string start, string goal ) 
{
    // std::cout << "AStar ! " << "\n";

    map< string, string > came_from;

    map< string, double > cost_so_far;

    PriorityQueue<string, double> frontier;
    frontier.put(start, 0);

    came_from[start] = start;
    
    cost_so_far.insert ( std::pair<string,double>(start,0) );

    NodeTrajectory path;
    // std::cout << "AStar : " << start << " --> " << goal << "\n";

    while (!frontier.empty()) {
        string current = frontier.get();
        // std::cout << "current : " << current << "\n";

        if (current == goal) {
            NodeTrajectory path;
            bool empty = false;
            string last = current;
            string newnode = current;
            path.insert( path.begin(), current );
            do {
                last = newnode;
                newnode = came_from[last];
                // std::cout << "  insert: " << came_from[last] << "\n";
                if(last != newnode)
                {
                    path.insert( path.begin(), came_from[last] );
                }
            } while (last != newnode);
            return path;
        }

        for (string next : graph.getNeighbors(current)) {
            // std::cout << "      neighbor : " << next << "\n";
            double new_cost = cost_so_far[current] + graph.cost(current, next);
            if (cost_so_far.find(next) == cost_so_far.end()
              || new_cost < cost_so_far[next]) {
                // cost_so_far[next] = new_cost;
                cost_so_far.insert( std::pair<string, double>(next,new_cost) );
                double priority = new_cost + heuristic(next, goal);
                frontier.put(next, priority);
                came_from.insert( std::pair<string,string>(next,current) );
                // came_from[next] = current;
                // std::cout << "came_from[ " << next << " ] = " << current << "\n";
            }
        }
    }
    std::cout<< "\n\nAStar FAILURE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n";
    return path;
}

void AStar::addNode( string id, int x, int y, int yaw, string neighbor )
{
    if( graph.nodes.find(id) != graph.nodes.end() )
    {
        std::cout << "ERROR ! Can't add a node when another node already exists with the same ID : " << id << "\n";
        return;
    }
    Node n = Node(id, x, y, yaw);
    graph.nodes[id] = n;
    Edge e = Edge(id, neighbor);
    graph.edges.push_back(e);
}

void AStar::deleteNode( string n )
{
    for (auto it = graph.edges.begin(); it != graph.edges.end(); )
    {
        if( it->n1 == n | it->n2 == n )
        {
            it = graph.edges.erase(it);
        }
        else {
            ++it;
        }
    }
    for (auto it = graph.nodes.begin(); it != graph.nodes.end(); )
    {
        if( it->first == n | it->first == n )
        {
            it = graph.nodes.erase(it);
        }
        else {
            ++it;
        }
    }
}

// double AStar::distance( )