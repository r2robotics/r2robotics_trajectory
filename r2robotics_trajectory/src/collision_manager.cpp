#include "collision_manager.h"


using namespace std;
using namespace fcl;

CollisionManager::CollisionManager() {
}

CollisionManagerPtr_t CollisionManager::create ()
{
      CollisionManagerPtr_t shPtr (new CollisionManager ());
      return shPtr;
}

void CollisionManager::addRobotPart( CollisionPartPtr_t part )
{
    robotParts.push_back( part );
    // std::cout << "Robot part added" << "\n";
}

void CollisionManager::addEnvPart( CollisionPartPtr_t part )
{
    environmentParts.push_back( part );
}

void CollisionManager::setRobotPose( double x, double y, double yaw )
{
    setRobotPose( Pose(x,y,yaw) );
}

void CollisionManager::setRobotPose( Pose p )
{
    currentRobotPose = poseToTransform( p );
}

TransformD CollisionManager::poseToTransform( Pose pose )
{
    return ToTransform(pose.x, pose.y, 0, pose.yaw);
}

TransformD CollisionManager::ToTransform( double x, double y, double z, double theta )
{
    TransformD tf;
    tf.setIdentity();
    tf.translation() = Vector3<double>(x,y,z);
    double qz = sin( DEG2RAD( theta/2.0 ) );
    double q0 = cos( DEG2RAD( theta/2.0 ) );
    tf.linear() = Quaternion<double>(q0, 0, 0, qz).toRotationMatrix();
    return tf;
}

Pose CollisionManager::getDistancesToObstacles( Pose pose ) {
    double min_distance = numeric_limits<double>::max();
    Pose min_Pose;
    for(auto const& robot_part: robotParts) {
        for(auto const& env_part: environmentParts) {
            TransformD robottf = poseToTransform( pose );
            TransformD new_robot_pose = TransformD::Identity();
            new_robot_pose.translation() = robot_part->pose.translation() + robottf.translation();
            new_robot_pose.linear() = robottf.linear();
            CollisionObjectD* robot_obj = new CollisionObjectD(
                robot_part->geometry, new_robot_pose);
            CollisionObjectD* env_obj = new CollisionObjectD(
                env_part->geometry, env_part->pose);
            DistanceRequestD request;
            DistanceResultD result;
            distance(robot_obj, env_obj, request, result);
            if( result.min_distance < min_distance )
            {
                min_distance = result.min_distance;
                min_Pose.x = result.nearest_points[1][0] - result.nearest_points[0][0];
                min_Pose.y = result.nearest_points[1][1] - result.nearest_points[0][1];
            }
        }
    }
    return min_Pose;
}



double CollisionManager::getDistanceToObstacles( Pose pose ) {
    // std::cout << "Starting calculating distance to obstacles ..." << "\n";
    double min_distance = numeric_limits<double>::max();
    for(auto const& robot_part: robotParts) {
        // std::cout << "robot : " << robot_part->name << "\n";
        for(auto const& env_part: environmentParts) {
            // std::cout << "env : " << env_part->name << "\n";
            TransformD robot_tf = poseToTransform( pose );
            TransformD new_robot_pose = TransformD::Identity();
            new_robot_pose.translation() = robot_part->pose.translation() + robot_tf.translation();
            new_robot_pose.linear() = robot_tf.linear();
        
            detail::GJKSolver_indep<double> solver;
            bool res;
            double dist;
            Vector3<double> closest_p1, closest_p2;

            if( robot_part->type == "box" )
            {
                if( env_part->type == "box" )
                {
                    res = solver.template shapeDistance<Box_t, Box_t>( *robot_part->box, 
                    new_robot_pose, *env_part->box, env_part->pose, &dist, &closest_p1, &closest_p2); 
                }
                if( env_part->type == "cylinder" )
                {
                    res = solver.template shapeDistance<Box_t, Cylinder_t>( *robot_part->box, 
                    new_robot_pose, *env_part->cylinder, env_part->pose, &dist, &closest_p1, &closest_p2);
                }
                if( env_part->type == "half_space")
                {
                    res = solver.template shapeDistance<Box_t, Halfspace_t>( *robot_part->box,
                    new_robot_pose, *env_part->half_space, env_part->pose, &dist, &closest_p1, &closest_p2);
                }
            }
            if( robot_part->type == "cylinder" )
            {
                if( env_part->type == "box" )
                {
                    res = solver.template shapeDistance<Cylinder_t, Box_t>( *robot_part->cylinder,
                    new_robot_pose, *env_part->box, env_part->pose, &dist, &closest_p1, &closest_p2);

                }
                if( env_part->type == "cylinder" )
                {
                    res = solver.template shapeDistance<Cylinder_t, Cylinder_t>( *robot_part->cylinder,
                    new_robot_pose, *env_part->cylinder, env_part->pose, &dist, &closest_p1, &closest_p2);
                }
                if( env_part->type == "half_space")
                {
                    res = solver.template shapeDistance<Cylinder_t, Halfspace_t>( *robot_part->cylinder,
                    new_robot_pose, *env_part->half_space, env_part->pose, &dist, &closest_p1, &closest_p2);
                }
            }
            // std::cout << dist << "\n";
            if( dist < min_distance )
            {
                min_distance = dist;
            }
        }
    }
    return min_distance;
}

bool CollisionManager::testCollision( Pose p ) {
    for(auto const& robot_part: robotParts) {
        for(auto const& env_part: environmentParts) {
            TransformD robottf = poseToTransform( p );
            TransformD new_robot_pose = TransformD::Identity();
            new_robot_pose.translation() = robot_part->pose.translation() + robottf.translation();
            new_robot_pose.linear() = robottf.linear();

            CollisionObjectD* robot_obj = new CollisionObjectD(
                robot_part->geometry, new_robot_pose);
            CollisionObjectD* env_obj = new CollisionObjectD(
                env_part->geometry, env_part->pose);

            CollisionRequestD request;
            CollisionResultD result;
            collide(robot_obj, env_obj, request, result);

        }
    }
}